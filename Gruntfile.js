/*jshint esversion: 6*/
module.exports = grunt => {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'dist/main.css': 'styles/main.scss'
        }
      }
    },
    jshint: {
      all: ['javascripts/*.js']
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['javascripts/*.js'],
        dest: 'dist/main.js',
      },
    },
    watch: {
      scripts: {
        files: ['javascripts/*.js'],
        tasks: ['jshint', 'concat']
      },
      styles: {
        files: ['styles/*.scss'],
        tasks: ['sass']
      }
    }
  });

  grunt.registerTask('default', ['sass', 'jshint', 'concat']);
};
