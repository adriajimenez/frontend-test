# Frontend Test

## Test Information

Average completion time should be 15 to 30 minutes. After 30 minutes if you haven't finished don't worry. This test is to see your knowledge and care level about coding.

Please make the test with your best knowledge and like if this was some work to deliver. That will provide a real value of your work.

## Project Information

The project is simple, 1 HTML file, 1 Scss file and 1 js file.  

**How to setup the project**

First install latest Node version [from here](http://www.nodejs.org).

1. Run `npm install` to install project dependencies.
2. Run `grunt` to compile sass or `run watch` to compile while working.

Jquery is included in the project but commented. If you feel you need it, uncomment it.

# Test Requirements

Everything should work with latest version of Chrome or Firefox. Forget about IE.
Don't be afraid to do commits in the project.

Please complete the following using your best skills:

[ ] You will see a placeholder image. Please replace the image for the appropiate markup and styles.  
[ ] The copyright in the footer is missaligned. Please fix it.  
[ ] Make the colors of headings and texts configurables via Scss variables.  
[ ] Form should validate that is an email, and password should be 8 characters long.  
[ ] The Sign in button should be hidden and only appear once both fields have been filled.  
[ ] Once the form is submitted, the "Hello, world!" title should change to contain the filled email. Example: "Hello test@gmail.com"